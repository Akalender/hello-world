#!/bin/bash

docker build -t hello-world-image .

docker save hello-world-image:latest | sudo k3s ctr images import -

kubectl create namespace abdullah

kubectl apply -f hello-world-service.yaml --namespace=abdullah

kubectl apply -f hello-world-deployment.yaml --namespace=abdullah